
import UIKit

//Chapter3
//Mini exercises9
//4 & 5 For Loops && While loops

for _ in 0..<10 {
    print("I will not skip the fundamentals!")
}
var i = 1
while i <= 10 {
    print(i * i)
    i += 1
}
//1. Arrays
//1.1
var listNumbers = [1, 2, 3, 10, 100]
print(listNumbers.max()!)

// sau cu for loop
var max = listNumbers[0]
for number in listNumbers {
    if number > max {
        max = number
    }
}
print(max)

//1.2
for number in listNumbers {
    if number % 2 == 1 {
        print(number)
    }
}

//1.3
var sum = 0
for number in listNumbers {
    sum += number
}
print(sum)

//2. Dictionaries
//2.1
var interns = ["Adina": 23, "Abel": 22, "Alex": 22]
for (name, age) in interns {
    print("\(name): \(age)")
}

//2.2
interns.removeValue(forKey: "Abel")
interns["Ion"] = 60

//2.3
interns.removeValue(forKey: "Ion")
//Complex exercises
//4&5. For Loops && While loops
//1
var index = 1
var n = 5
while index <= n {
    if index == n {
        print(index)
    } else {
        print(index)
        print(n)
    }
    index += 1
    n -= 1
}

//2
n = 4
for index in 1...n {
    if index == 1 || index == n {
        var index = 1
        while index <= n {
            if index == n {
                print("*")
            } else {
                print("*", terminator: "")
            }
            index += 1
        }
    } else {
        var index = 1
        while index <= n {
            switch index {
            case 1:
                print("*", terminator: "")
            case n:
                print("*")
            default:
                print(" ", terminator: "")
            }
            index += 1
        }
    }
}

//3
n = 4
var m = 7
for index in 1...n {
    if index == 1 || index == n {
        var index = 1
        while index <= m {
            if index == m {
                print("*")
            } else {
                print("*", terminator: "")
            }
            index += 1
        }
    } else {
        var index = 1
        while index <= m {
            switch index {
            case 1:
                print("*", terminator: "")
            case m:
                print("*")
            default:
                print(" ", terminator: "")
            }
            index += 1
        }
    }
}


//4
var number = 1234
var reversedNumber = 0
while number != 0 {
    reversedNumber = reversedNumber * 10 + number % 10
    number /= 10
}
print(reversedNumber)

//5
var a = 24
var b = 18
var greatestCommonDivisor = 0
if(a>b) {
    max = a
} else {
    max = b
}
for index in 1...max/2 {
    if a % index == 0 && b % index == 0 {
        greatestCommonDivisor = index
    }
}
print(greatestCommonDivisor)

//5
number = 17
var isPrime = "prime"
for divisor in 2...number/2 {
    if number % divisor == 0 {
        isPrime = "not prime"
    }
}
print(isPrime)

//1.Arrays
//1.1
index = 0
var listOfNumbers = [1, 9, 2, 8, 3, 7, 4, 6, 5, 0]
while index < listOfNumbers.count {
    if index % 2 == 1 {
        print(listOfNumbers[index])
    }
    index += 1
}

//1.2
for index in 0..<listOfNumbers.count {
    if index % 2 == 1 {
        print(listOfNumbers[index])
    }
}

//1.3
for index in 0..<listOfNumbers.count {
    print(listOfNumbers[listOfNumbers.count - index.hashValue - 1])
}

//1.4
var lastIndex = listOfNumbers.count - 1
for firstIndex in 0..<listOfNumbers.count / 2 {
    let aux = listOfNumbers[firstIndex]
    listOfNumbers[firstIndex] = listOfNumbers[lastIndex - firstIndex]
    listOfNumbers[lastIndex - firstIndex] = aux
}
print(listOfNumbers)

//1.5
for index in 0..<listOfNumbers.count - 1 {
    for index2 in index + 1..<listOfNumbers.count {
        if listOfNumbers[index] < listOfNumbers[index2] {
            let aux = listOfNumbers[index]
            listOfNumbers[index] = listOfNumbers[index2]
            listOfNumbers[index2] = aux
        }
    }
}
print(listOfNumbers)

//2.Dictionaries
//2.1
var message = "hello world"
var code = [ "a" : "b", "b" : "c", "c" : "d", "d" : "e", "e" : "f", "f" : "g", "g" : "h", "h" : "i", "i" : "j", "j" : "k", "k" : "l", "l" : "m", "m" : "n", "n" : "o", "o" : "p", "p" : "q", "q" : "r", "r" : "s", "s" : "t", "t" : "u", "u" : "v", "v" : "w", "w" : "x", "x" : "y", "y" : "z", "z" : "a" ]
var encodeMessage = ""
for letter in message.characters {
    for (key, value) in code {
        if String(letter) == key {
            encodeMessage += value
        }
    }
}
print(encodeMessage)

//2.2
var dict1 = ["firstName": "Alexandru", "lastName": "Vescan"]
var dict2 = ["firstName": "Adina", "lastName": "Sabadis"]
var dict3 = ["firstName": "Abel", "lastName": "Pazsint"]
var arrayOfDictionaries = [dict1, dict2, dict3]
var firstNames: [String] = []
for dict in arrayOfDictionaries {
    for (key, value) in dict {
        if key == "lastName" {
            firstNames.append(value)
        }
    }
}
print(firstNames)

//2.3
var fullNames: [String] = []
var fullName = ""
for dict in arrayOfDictionaries {
    for (_, value) in dict {
        fullName += value + " "
    }
    fullName.removeLast()
    fullNames.append(fullName)
    fullName = ""
}
print(fullNames)

//7&8. Tuples && Enums
//1
var originalLocation = (x: 0, y: 0)
var finalLocation = (x: 0 ,y: 0)
enum Steps {
    case up, down, right, left
}
var steps: [Steps] = [.up, .right, .right, .right, .up, .up, .right, .right, .up, .left, .left, .left, .left, .down]
for step in steps {
    switch step {
    case .up:
        finalLocation.x += 1
    case .down:
        finalLocation.x -= 1
    case .right:
        finalLocation.y += 1
    case .left:
        finalLocation.y -= 1
    }
}
print(finalLocation)

//2
var tuple1 = (5, 8)
var tuple2 = (17, 9)
var numerator = tuple1.0 * tuple2.1 + tuple2.0 * tuple1.1
var denominator = tuple1.1 * tuple2.1
var tupleSum = (numerator, denominator)

//3
enum CoinType: Int {
    case penny = 1
    case nickle = 5
    case dime = 10
    case quarter = 25
}
var moneyArray:[(Int,CoinType)] = [(10, .penny), (15, .nickle), (3, .quarter), (20, .penny), (3, .dime)]
sum = 0
for money in moneyArray {
        sum += money.0 * money.1.rawValue
}
print(sum)
