
import UIKit

//Chapter4
//Mini exercises
//1&2. Functions && Optionals
//1
func min2(_ a: Int, _ b: Int) -> Int {
    return a < b ? a : b
}

//2
func lastDigit(_ number: Int) -> Int {
    return number % 10
}

//3. Closures
//1
func applyKTimes(K: Int, closure: () -> Void) {
    for _ in 0..<K {
        closure()
    }
}

//Complex exercises
//1&2. Functions && Optionals
//1
func first(_ N: Int) -> [Int] {
    var firstNNumbers = [Int]()
    for number in 1...N {
        firstNNumbers.append(number)
    }
    return firstNNumbers
}

//2
func countdown(_ N: Int) {
    var n = N
    while n > 0 {
        print(n)
        sleep(1)
        n -= 1
    }
    print("GO")
}

//3
func divides(_ a: Int, _ b: Int) -> Bool {
    if a % b == 0 {
        return true
    }
    return false
}

func countDivisors(_ number: Int) -> Int {
    var numberOfDivisors = 0
    if number > 1 {
        for divisor in 2...number {
            if divides(number, divisor) == true  && divisor != number {
                numberOfDivisors += 1
            }
        }
    }
    return numberOfDivisors
}

func isPrime(_ number: Int) -> Bool {
    if countDivisors(number) > 0 {
        return false
    }
    return true
}

//4
func printFirstPrimes(_ count: Int) {
    var number = 2
    var numberOfPrimes = 0
    while numberOfPrimes < count {
        if isPrime(number) {
            numberOfPrimes += 1
            print(number)
        }
        number += 1
    }
}

printFirstPrimes(7)

//5
func returnNumberOfTimes(_ a: Int, _ b: Int) -> Optional<Int> {
    var numberOfTimes: Int?
    if divides(a, b) {
        numberOfTimes = a / b
    }
    return numberOfTimes
}

func divideIfWhole(numberOfTymes: (Int, Int) -> Optional<Int>, _ a: Int, _ b: Int) {
    if let unwrappedOprional = numberOfTymes(a,b) {
        print("Yep, it divides \(unwrappedOprional) times")
    } else {
        print("Not divisible!")
    }
}

//6
func pushQueue(_ number: Int, queue: inout [Int]) {
    queue.append(number)
}

func popQueue(queue: inout [Int]) -> Optional<Int> {
    var number: Int?
    if queue.count > 0 {
        number = queue[0]
        queue.remove(at: 0)
    }
    return number
}

//7
func pushStack(_ number: Int, stack: inout [Int]) {
    stack.insert(number, at: 0)
}

func popStack(stack: inout [Int]) -> Optional<Int> {
    var number: Int?
    if stack.count > 0 {
        number = stack[0]
        stack.remove(at: 0)
    }
    return number
}

func topStack(stack: [Int]) -> Optional<Int> {
    var number: Int?
    if stack.count > 0 {
        number = stack[0]
    }
    return number
}

//3. Closures
//1
var isWhole = { (number: Int) -> Bool
    in
    if number >= 0 {
        return true
    }
    return false
}

func filterInts(ints: inout [Int], closure: (Int) -> Bool) {
    var index = 0
    for number in ints {
        if !closure(number) {
            ints.remove(at: index)
            index -= 1
        }
        index += 1
    }
}

//2
var concatenate = { (strings: [String]) -> String
    in
    var concatenatedStrings = ""
    for string in strings {
        concatenatedStrings += string
    }
    return concatenatedStrings
}

func joinStrings(strings: [String], closure: ([String]) -> String) {
    print(closure(strings))
}

//3
var add = { (a: Int, b: Int) -> Int
    in
    return a + b
}

func combineArrays(array1: [Int], array2: [Int], closure: (Int, Int) -> Int) -> [Int] {
    var combinedArray = [Int]()
    var index = 0
    while index < array1.count {
        combinedArray.append(closure(array1[index], array2[index]))
        index += 1
    }
    return combinedArray
}


