//Chpater2

import UIKit
//Mini exercises

//2. Operators in swift
//2.1
var testNumber = 34
var evenOdd = testNumber % 2
testNumber = 23
evenOdd = testNumber % 2
testNumber = 73
evenOdd = testNumber % 2
testNumber = 4
evenOdd = testNumber % 2
//Cu operatorul mod (%) calculezi restul impartirii intregi

//2.2
var answer = 0
answer += 1
answer += 10
answer *= 10
answer /= 5

//After all these operations, the answer is 22

//3. Work with numbers
//3.1
let myAge = 22

//3.2
var averageAge = Double(myAge)
var adinaAge = 23
var ableAge = 22
averageAge = Double(adinaAge + ableAge) / 2

//4. Booleans
//4.1

let isTeenager = (myAge >= 13 && myAge <= 19)

//4.2
let theirAge = 28
let amIATeenager = (myAge >= 13 && myAge <= 19)
let isHeATeenager = (theirAge >= 13 && theirAge <= 19)
let bothTeenagers = (amIATeenager && isHeATeenager)

//5. Use Strings
//5.1
var firstName = "Alexandru"
var lastName = "Vescan"
var fullName = firstName + " " + lastName
var fullNameAndAge = "Full name: \(fullName), Age: \(myAge)"
print(fullNameAndAge)

//Complex exercises
//2.1
var age = 32
var isAdult = (age >= 19)

var number = -3
var number2 = (number < 0 ? number*2 : 0)

var name = "Batman"
var isBatman = (name == "The Doctor")

//2.2
var closedRange = 2...13
var halfOpenRange = 2..<9

//2.3
var comparisonOperators = "==, <, >, >=, <=, !="
number = 16
let equals = (number == 16)
let greaterThen = (number > 66)
let lessThan = (number <= 5)
let greaterThanOrEquals = (number >= 55)
let lessThanOrEquals = (number <= 16)
let notEquals = (number != 3)

//2.4
var a = 11
var b = 24
let largestOne = (a > b ? a : b)
print("The largest number between \(a) and \(b) is \(largestOne)")

//2.5
number = 2
let evenOrOdd = (number % 2 == 0 ? "even" : "odd")
print(evenOrOdd)

//2.6
a = 12
b = 3
let divisible = (a % b == 0 ? "divisible" : "not divisible")

//2.7
a = 2
b = 3
var c = 2
var condition = (a == b || a == c || b == c)
let atLeastTwo = (condition ? "At least two variables have the same value" : "All the values are different")
print(atLeastTwo)

//2.8
var year = 2014
var divisibleBy4 = (year % 4 == 0)
var divisibleBy100And400 = (year % 100 == 0 && year % 400 == 0)
let leapYear = (divisibleBy4 || divisibleBy100And400 ? "Leap year!" : "Not a leap year!")
print(leapYear)

//3. Work with numbers
//3.1
var finalsGrade = 2.0, midtermGrade = 4.0, projectGrade = 3.0
let classGrade = 1 / 2 * finalsGrade + 1 / 5 * midtermGrade + 3 / 10 * projectGrade
print(classGrade)

//3.2
var mealCost = 16.5
var tip = 20
var totalCost = mealCost + (mealCost * Double(tip)) / 100
print("The total cost of the meal is \(totalCost)")

//3.3
var doubleNumber = 5.1517
var roundedNumber = Int(doubleNumber * 10)
var rounded = Double(roundedNumber) / 10

//3.4
var grade1 = 7.0
var grade2 = 9.0
var grade3 = 5.0
var yourGrade = 8.0
var average = (grade1 + grade2 + grade3 + yourGrade)/4
print(yourGrade > average ? "Above average" : "Below average")

//4.Booleans
//4.1
let answer1 = true && true //true and true = true
let answer2 = false || false //false or false = false
let answer3 = (true && 1 != 2) || (4 > 3 && 100 < 1) //true or false = true

//5.Use strings
//5.1
let value = 10
let multiplier = 5
let sum = "\(value) multiplied by \(multiplier) equals \(value * multiplier)" //sum = 50

//5.2
var aString = "Replace the e letter with *"
var replaceString = ""
//var i = 0
for letter in aString {
    if letter == "e" {
//        let x = aString.index(aString.startIndex, offsetBy: i)..<aString.index(aString.startIndex, offsetBy: i+1)
//        aString.removeSubrange(x)
//        aString.insert("*", at: aString.index(aString.startIndex, offsetBy: i))
        replaceString.append("*")
    } else {
        replaceString.append(letter)
    }
    //i += 1
}
print(replaceString)

//5.3
aString = "Hello Swift"
var reverse = ""
for letter in aString {
    reverse.insert(letter, at: reverse.startIndex)
}
print(reverse)

//for (index, value) in aString.enumerated() {
//
//}

//5.4
print(aString == reverse)

