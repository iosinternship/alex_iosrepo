
import UIKit

//Chapter5
//1
struct Location {
    let x: Int
    let y: Int
}

struct Ship {
    let origin: Location
    let length: Int
    enum Direction {
        case up, down, left, right
    }
    let direction: Direction
    
    func shot(_ point: Location) -> String {
        switch self.direction {
        case .up:
            if point.x >= origin.x - length - 1 && point.x <= origin.x {
                return "hit"
            }
            return "miss"
        case .down:
            if point.x <= origin.x - length - 1 && point.x >= origin.x {
                return "hit"
            }
            return "miss"
        case .left:
            if point.y >= origin.y - length - 1 && point.y <= origin.y {
                return "hit"
            }
            return "miss"
        case .right:
            if point.x <= origin.y - length - 1 && point.y >= origin.y {
                return "hit"
            }
            return "miss"
        }
    }
}

struct Board {
    let length = 10
    var ships: [Ship] = []
    
    func isOnBoard(_ ship: Ship) -> Bool {
        switch ship.direction {
        case .up:
            return ship.origin.x - ship.length >= 0
        case .down:
            return ship.length + ship.origin.x - 1 <= length
        case .left:
            return ship.origin.y - ship.length >= 0
        case .right:
            return ship.length + ship.origin.y - 1 <= length
        }
    }
    
    mutating func addShipOnBoard(_ ship: Ship) {
        if isOnBoard(ship) {
            ships.append(ship)
            print("Ship added")
        } else {
            print("Ship not added")
        }
    }
    
    func fire(_ location: Location,_ board: Board) -> String {
        for ship in board.ships {
            if ship.shot(location) == "hit" {
                return "hit"
            }
        }
        return "miss"
    }
}

var board1 = Board()
var ship1 = Ship(origin: Location(x: 10, y: 10), length: 2, direction: .left)
board1.addShipOnBoard(ship1)

var board2 = Board()
var ship2 = Ship(origin: Location(x: 10, y: 10), length: 2, direction: .up)
board2.addShipOnBoard(ship2)

print(board1.fire(Location(x: 1, y: 10), board2))
print(ship1)
print(ship2)

//2
class List {
    let name: String
    var titles: [String] = []
    
    init(_ name: String, _ titles: [String]) {
        self.name = name
        self.titles = titles
    }
    
    func printList() {
        for title in titles {
            print(title)
        }
    }
}

class User {
    let name: String
    var lists: [String: List] = [:]
    
    init(name: String) {
        self.name = name
    }
    
    func addList(list: List) {
        lists[name] = list
    }
    
    func listForName(name: String) -> List? {
        let optionalList: List?
        optionalList = lists[name]
        if let unwrappedOptional = optionalList {
            return unwrappedOptional
        } else {
            return nil
        }
    }
}

let alex = User(name: "Alex")
let adina = User(name: "Adina")
let list = List("Movies", ["Movie1", "Movie2"])
alex.addList(list: list)
adina.addList(list: list)
print()
alex.listForName(name: alex.name)?.printList()
print()
adina.listForName(name: adina.name)?.printList()
print()
alex.lists[alex.name]?.titles.append("Movie3")
alex.listForName(name: alex.name)?.printList()
print()
adina.lists[adina.name]?.titles.append("Movie4")
adina.listForName(name: adina.name)?.printList()
print()
print(alex)
print(adina)
/* All the changes have been made.
 If you implement the same with structs, you can't modify the same list, a copy will be created.
*/

//3
/*TShirt, Adress and ShoppingCart are structs because they are value types. When you add a shirt in the cart, a new adress, or a shopping cart,
you make copies. You store unique values for every user. StoreUser is class because it may be inherited, for example, by a facebookUser, who created his account whit facebook.
 */
struct TShirt {
    enum Size {
        case XS, S, M, L, XL, XXL, XXXL
    }
    let size: Size
    let color: String
    let price: Double
}

struct Adress {
    let number: Int
    let street: String
    let city: String
    let zipCode: Int
}

class ShoppingCart {
    let tShirts: [TShirt]
    let adress: Adress
    var totalCost: Double {
        get {
            var sum = 0.0
            for tshirt in tShirts {
                sum += tshirt.price
            }
            return sum
        }
    }
    
    init(tShirts: [TShirt], adress: Adress) {
        self.tShirts = tShirts
        self.adress = adress
    }
}

class StoreUser {
    let name: String
    let mail: String
    let shoppingCart: ShoppingCart
    
    init(name: String, mail: String, shoppingCart: ShoppingCart) {
        self.name = name
        self.mail = mail
        self.shoppingCart = shoppingCart
    }
}
let tShirt1 = TShirt(size: .S, color: "red", price: 39.9)
let tShirt2 = TShirt(size: .S, color: "blue", price: 45)
let tShirt3 = TShirt(size: .S, color: "green", price: 70.3)
let adress = Adress(number: 35, street: "Bucuresti", city: "Cluj-Napoca", zipCode: 400202)
let tShirts = [tShirt1, tShirt2, tShirt3]
let shoppingCart = ShoppingCart(tShirts: tShirts, adress: adress)
let user = StoreUser(name: "Mirela", mail: "mirela@mail.com", shoppingCart: shoppingCart)

//4
class Operation {
    let value: Double
    
    init(_ value: Double) {
        self.value = value
    }
    
    func performOperation() -> Double {
        fatalError()
    }
}

class Add: Operation {
    override func performOperation() -> Double {
        return self.value
    }
}

class Substract: Operation {
    override func performOperation() -> Double {
        return -self.value
    }
}

class Calculator {
    var val = 0.0
    var operations: [Operation]
    
    init(_ operations: [Operation]) {
        self.operations = operations
    }
    
    func calculate() {
        for operation in operations {
            val += operation.performOperation()
            print(val)
        }
    }
}

var operations: [Operation]
operations = [Add(4), Substract(1), Add(16)]
Calculator(operations).calculate()

